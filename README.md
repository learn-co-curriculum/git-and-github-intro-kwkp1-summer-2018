# You will be able to

1. Explain what **Git** and **GitHub** are and what they do
2. Use GitHub to **fork a repository**, then **clone it locally** (on your own computer)

# Git + GitHub

**Git** and **GitHub** are essential tools for saving and sharing your code. When a group of people get together to write a program, they all have to work on the same files. But what's the best way to deal with the confusion that might arise when multiple people are changing code in the same file? That's where Git and GitHub come in.

Whenever you get your program working properly, it's important to save that version of the program, right? The cool thing about Git is that this tool remember exactly what your code looked like at each moment you choose to save it. It's kind of like Git takes a snapshot of your code whenever you want it to. Imagine you are coding along, and suddenly EVERYTHING is broken. What would you do? If you used Git and saved your work along the way, you could essentially rewind your code history back to a working version.

[GitHub](https://github.com) is a place to keep your code beyond your own computer. After you push your code to GitHub, you can then access it from other computers as well as allow collaborators to suggest changes or fixes. You can also contribute to code other people have pushed to the site themselves.

So we use **Git** to keep track of our software versions, and we use **GitHub** to build software with team or community members.
